import os,sys
import re

f = open(sys.argv[1])
ext = int(sys.argv[2])
firstOnly = False
if len(sys.argv) > 3:
    firstOnly = True

out_name, extension = os.path.splitext( sys.argv[1] )

if firstOnly:
    out_name = out_name + "ex%d.firstonly.bed"%ext
    print firstOnly
else:
    out_name = out_name + "ex%d.bed"%ext

out = open(out_name, 'w')
starts = set()
seen = set()
for l in f:
    l = l.strip()
    if l[0] == '#':
        continue
    else:
        tokens = l.split('\t')
        if not re.match( "^chr(\d+|X)$", tokens[2]):
            continue

        if firstOnly:
            if tokens[1] in seen:
                #print tokens[:4]
                continue
            else:
                seen.add(tokens[1])

        if tokens[3] != '-':
            starts.add("%s_%s" % (tokens[2], int(tokens[4])))
        else:
            starts.add("%s_%d" % (tokens[2], int(tokens[5]) ))
f.close()
starts = list(starts)
starts.sort(key = lambda k: (k.split('_')[0], int(k.split('_')[1])))
for s in starts:
    chrom, start = s.split('_')
    start = int(start)
    out.write("\t".join( map(str, [chrom, start - ext, start + ext] )))
    out.write('\n')
out.close()

