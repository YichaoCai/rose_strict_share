

import os,sys
import numpy as np
import pysam
import argparse
import re
from collections import defaultdict
import subprocess
import shlex

def parseArgs():
    parser = argparse.ArgumentParser("Stitch peaks and call super-enhancer.")

    parser.add_argument( "input",
                        help = "The input file in BED format.")
    parser.add_argument("ref",
                        help = "The reference build. Can be [hg19, hg18, mm9, mm8].")
    parser.add_argument( "bam",
                        help = "The bam file to calculate the signal.")
    parser.add_argument("-c", "--control", dest="control",
                        help = "The control bam file to calculate background signal.")
    parser.add_argument("-o", "--output", dest="output", default="New",
                        help = "The output prefix, default is 'New'.")
    parser.add_argument("-f", "--fraction", dest="fraction", default=0, type=float,
                        help = "The overlapping fraction (only those with > fraction will be retained.")
    parser.add_argument('-w', '--window', dest='window', default=12500, type=int,
                        help = "The window for stitching peaks. Default: 12500")
    parser.add_argument('-e', '--exclude', dest='exclude', default=2000, type=int,
                        help = "The size of exclusion zones (on each side) around the TSS. Default: 2000")
    parser.add_argument('--separate', dest='separate', default=False, type=bool,
            help = "Whether to separate Super-enhancers and typical-enhancers based on their proximity to TSSes. Default: False:")

    args = parser.parse_args()
    return args



class Locus:
    def __init__(self, chrom, start, end, strand, name, commonName = None, signal = None, ctrlSignal = None):
        '''
        strand should be one of ["+", "-", "."]
        '''
        self.__chrom = chrom
        self.__start = int(start)
        self.__end = int(end)
        if strand not in ['+', '-', '.']:
            raise ValueError("strand should be one of [+, -, .]")
        self.__strand = strand
        self.__name = name
        self.__commonName = commonName
        self.__signal = signal  #Average sianl
        self.__ctrlSignal = ctrlSignal #Average ctrl signal

    def getStart(self):
        return self.__start

    def getEnd(self):
        return self.__end

    def getLen(self):
        return self.getEnd() - self.getStart()

    def getChrom(self):
        return self.__chrom

    def getStrand(self):
        return self.__strand

    def getName( self ):
        return self.__name

    def setCommonName(self, cn):
        self.__commonName = cn

    def getCommonName(self):
        return self.__commonName

    def overlap(self, otherLocus, forceStrand = False, otherFraction = 0):
        if self.getChrom() != otherLocus.getChrom():
            return False
        elif forceStrand and self.getStrand() != otherLocus.getStrand():
            return False
        else:
            return (min( self.getEnd(), otherLocus.getEnd() ) - max( self.getStart(), otherLocus.getStart() )) * 1.0 / otherLocus.getLen() > max(otherFraction,0)

    def setSignal( self, signal ):
        '''
        The average sianal
        '''
        self.__signal = signal

    def setCtrlSignal( self, ctrlSignal ):
        self.__ctrlSignal = ctrlSignal


class StitchedLocus:
    def __init__( self, loci, window, signal = None, ctrlSignal = None, consSignal = None, consCtrlSignal = None ):
        if len( loci ) <= 0:
            raise ValueError("No Locus")
        self.__loci = loci
        self.__signal = signal
        self.__ctrlSignal = ctrlSignal
        self.__consSignal = consSignal
        self.__consCtrlSignal = consCtrlSignal
        self.__window = window

    def getLoci( self ):
        return self.__loci

    def getSignal( self ):
        return self.__signal

    def getStart( self ):
        return self.__loci[0].getStart()

    def getEnd( self ):
        return self.__loci[-1].getEnd()

    def setSignal( self, signal ):
        self.__signal = signal

    def setCtrlSignal( self, ctrlSignal ):
        self.__ctrlSignal = ctrlSignal

    def setConsSignal( self, consSignal ):
        self.__consSignal = consSignal

    def setConsCtrlSignal( self, consCtrlSignal ):
        self.__consCtrlSignal = consCtrlSignal

    def getConsSignal( self, consSignal ):
        return self.__consSignal

    def getTotalConsSize( self ):
        totalSize = 0
        for l in self.getLoci():
            totalSize += l.getLen()
        return totalSize

    def getSignal( self ):
        return self.__signal

    def getCtrlSignal( self ):
        return self.__ctrlSignal

    def getNumLoci( self ):
        return len(self.__loci)

    def getTotalConsSignal( self ):
        if self.__consSignal == None:
            return 0
        else:
            return sum(self.__consSignal)

    def getTotalConsCtrlSignal( self ):
        if self.__consCtrlSignal == None:
            return 0
        else:
            return sum(self.__consCtrlSignal)

    def getConsCtrlSignal( self ):
        return self.__consCtrlSignal

    def getName( self ):
        return "%d_%s_%dk_stitched" % (self.getNumLoci(), self.getLoci()[0].getName(),int(self.__window/1000))


def loadBed( filename, header = False ):
    '''
    Returns a dictionary of sorted lists of locis.
    The keys of the dictionary are the chromosome names.
    '''
    f = open( filename )
    loci = {}
    if header:
        f.readline()
    for r in f:
        tokens = r.strip().split('\t')
        if tokens[0] not in loci:
            loci[ tokens[0] ] = []
        loci[ tokens[0] ].append( Locus(tokens[0], tokens[1], tokens[2], tokens[5], tokens[3] ) )
    f.close()
    for chrom in loci:
        loci[ chrom ].sort( key=lambda k: (k.getStart(), k.getEnd() ))
    return loci

def loadTSS( filename, header = False, extension = 0 ):
    '''
    Returns a dictionary of sorted lists of locis of TSS.
    The keys of the dictionary are the chromosome names.
    '''
    '''
    1. bin
    2. name
    3. chrom
    4. strand
    5. txStart
    6. txEnd
    7. cdsStart
    8. cdsEnd
    9. exonCount
    10. exonStarts
    11. exonEnds
    12. score
    13. name2
    14. cdsStartStat
    15. cdsEndStat
    16. exonFrames
    '''
    f = open( filename )
    if header:
        f.readline()
    loci = {}
    for r in f:
        tokens = r.strip().split('\t')
        if not re.match('^chr(\d+|X|Y)$', tokens[2]):
            continue
        if tokens[2] not in loci:
            loci[ tokens[2] ] = []
        if tokens[3] == '-':
            loci[ tokens[2] ].append( Locus(tokens[2], int( tokens[5] ) - 1 - extension, int( tokens[5] )+ extension, tokens[3], tokens[1], commonName = tokens[12] ) )
        else:
            loci[ tokens[2] ].append( Locus(tokens[2], int( tokens[4] ) - extension, int( tokens[4] ) + 1 + extension, tokens[3], tokens[1], commonName = tokens[12] ) )
    f.close()
    for chrom in loci:
        loci[ chrom ].sort( key=lambda k: (k.getStart(), k.getEnd() ))
    print loci.keys()
    return loci

def getStartEndLists( chromLoci ):
    '''
    Given a list of loci on the same chromosome sorted by coordinates,
    return one list of the start coordinates and
    one list of end coordinates.
    Note that there should not be a loci in the list being included
    in another loci in the list.
    '''
    starts = []
    ends = []
    for l in chromLoci:
        starts.append( l.getStart() )
        ends.append( l.getEnd() )
    return  starts, ends

def testOverlap( locus, refLoci, starts, ends, fraction ):
    '''
    Test whether locus is overlapping with any of refLoci by fraction of the locus.
    The starts and ends are the outputs of getStartEndLists().
    Returns a boolean result
    '''
    import bisect
    leftIdx = bisect.bisect_left( ends, locus.getStart() )
    rightIdx = bisect.bisect_right( starts, locus.getEnd() )
    if rightIdx <= leftIdx:
        return False
    else:
        for i in range(leftIdx, rightIdx):
            if refLoci[ i ].overlap( locus, otherFraction = fraction ):
                return True
        return False


def removeLociOverlapRef( loci, ref, fraction = 0 ):
    chroms = set(loci.keys()).intersection( set(ref.keys()) )
    result = {}
    removed = {}
    for chrom in chroms:
        refStarts, refEnds = getStartEndLists( ref[ chrom ] )
        result[ chrom ] = []
        removed[ chrom ] = []
        for locus in loci[ chrom ]:
            if not testOverlap( locus, ref[ chrom ], refStarts, refEnds, fraction ):
                result[ chrom ].append( locus )
            else:
                removed[ chrom ].append( locus )
    return result, removed



def stitchLoci( loci, stitchWindow = 0, tss = None ):
    '''
    Stitch loci, and if tss is given, will not span any tss.
    That is loci at different sides of tss will not be stitched.
    '''

    chroms = set(loci.keys())#.intersection( set(tss.keys()))
    if tss != None:
        chroms = chroms.intersection( set(tss.keys()))
    stitched = {}
    for chrom in chroms:
        chromStitched = []
        stitched[ chrom ] = []
        cLoci = loci[ chrom ]

        tempStitched = [ cLoci[0], ]
        lastEnd = cLoci[0].getEnd()
        for locus in cLoci[1:]:
            if locus.getStart() < lastEnd + stitchWindow:
                tempStitched.append( locus )
            else:
                chromStitched.append( StitchedLocus( tempStitched, stitchWindow ) )
                tempStitched = [ locus, ]
            lastEnd = locus.getEnd()
        chromStitched.append( StitchedLocus( tempStitched, stitchWindow ) )


        if tss != None:
            cTss = tss[ chrom ]
            i = 0
            j = 0
            while i < len( chromStitched ) and j < len( cTss ):
                if chromStitched[ i ].getEnd() <= cTss[ j ].getStart():
                    stitched[ chrom ].append( chromStitched[ i ] )
                    i += 1
                elif chromStitched[ i ].getStart() >= cTss[ j ].getEnd():
                    j += 1
                else:
                    for locus in chromStitched[ i ].getLoci():
                        stitched[ chrom ].append( StitchedLocus( [ locus, ], stitchWindow ) )
                    i += 1
        else:
            stitched[ chrom ] = chromStitched
    return stitched


def calculateSignalChrom( taskQ, outQ, processID, sam, MMR, extension, isCtrl ):
    for chrom, stitchedChrom in iter(taskQ.get, "STOP"):
        print processID, chrom
        ticker = 0
        for s in stitchedChrom:
            ticker += 1
            if ticker % 1000 == 0:
                print "process %d: %d" %(processID, ticker )
            tags = sam.fetch(reference = chrom, start = s.getStart() - extension, end = s.getEnd() )
            seenDict = {}
            changes = defaultdict(int)
            for t in tags:
                if t.seq not in seenDict:
                    seenDict[ t.seq ] = 1
                else:
                    continue
                if not t.is_reverse:
                    changes[ t.pos ] += 1
                    changes[ t.pos + extension ] -= 1
                else:
                    changes[ t.pos + t.qlen - extension ] += 1
                    changes[ t.pos + t.qlen] -= 1
            totalSig = 0
            tracker = 0
            consSignals = []
            tempConsSig = 0
            consI = 0
            for i in range( s.getStart() - extension, s.getEnd() ):
                if i >= s.getStart():
                    totalSig += tracker
                    if s.getLoci()[consI].getStart() <= i < s.getLoci()[consI].getEnd():
                        tempConsSig += tracker
                    elif i >= s.getLoci()[consI].getEnd():
                        consSignals.append(tempConsSig * MMR / (s.getLoci()[consI].getEnd() - s.getLoci()[consI].getStart()) )
                        tempConsSig = 0
                        consI += 1
                        if s.getLoci()[consI].getStart() <= i < s.getLoci()[consI].getEnd():
                            tempConsSig += tracker

                tracker += changes[ i ]
            den = totalSig * MMR / (s.getEnd() - s.getStart())

            if not isCtrl:
                s.setSignal( den )
                s.setConsSignal( consSignals )
            else:
                s.setCtrlSignal( den )
                s.setConsCtrlSignal( consSignals )
        #outQ.put( (chrom, stitchedChrom ))

def calculateSignal( stitched, rankby, extension, isCtrl = False):
    import pysam
    sam = pysam.Samfile( rankby, "rb" )

    MMR = 1e6 * 1.0 / sam.mapped
    print rankby, MMR, sam.mapped
    ticker = 0

    #from multiprocessing import Process, Queue, current_process, freeze_support
    #taskQ = Queue()
    #outQ = Queue()
    #NUM_PROCESSES = 4
    #processes = []
    #freeze_support()
    #count = 0
    #for chrom in stitched:
    #    print chrom
    #    count += 1
    #    taskQ.put((chrom, stitched[chrom]))
    processID = 1
    #for i in range( NUM_PROCESSES ):
    #    processes.append(Process( target = calculateSignalChrom,
    #            args = ( taskQ, outQ, processID, sam, MMR, extension, isCtrl  ) ))
    #    processes[-1].start()
    #    processID += 1
    #
    #for i in range( NUM_PROCESSES ):
    #    print "put stop ", i
    #    taskQ.put( "STOP" )
    #for i in range( NUM_PROCESSES ):
    #    processes[i].join()
    #print "DONE"

    for chrom in stitched:
        #ticker = 0
        for s in stitched[chrom]:
            ticker += 1
            if ticker % 1000 == 0:
                print "process %d: %d" %(processID, ticker )
            tags = sam.fetch(reference = chrom, start = s.getStart() - extension, end = s.getEnd() )
            changes = defaultdict(int)
            for t in tags:
                if not t.is_reverse:
                    changes[ t.pos ] += 1
                    changes[ t.pos + extension ] -= 1
                else:
                    changes[ t.pos + t.qlen - extension ] += 1
                    changes[ t.pos + t.qlen] -= 1
            totalSig = 0
            tracker = 0
            consSignals = []
            tempConsSig = 0
            consI = 0
            if len(changes.keys()) > 0:
                for i in range( min(min(changes.keys()), s.getStart()), s.getEnd() ):
                    if i >= s.getStart():
                        totalSig += tracker
                        if s.getLoci()[consI].getStart() <= i < s.getLoci()[consI].getEnd():
                            tempConsSig += tracker
                        elif i >= s.getLoci()[consI].getEnd():
                            consSignals.append(tempConsSig * MMR  ) # / (s.getLoci()[consI].getEnd() - s.getLoci()[consI].getStart())
                            tempConsSig = 0
                            consI += 1
                            if s.getLoci()[consI].getStart() <= i < s.getLoci()[consI].getEnd():
                                tempConsSig += tracker

                    tracker += changes[ i ]
                consSignals.append(tempConsSig * MMR  ) #/ (s.getLoci()[consI].getEnd() - s.getLoci()[consI].getStart())
            den = totalSig * MMR #/ (s.getEnd() - s.getStart())

            if not isCtrl:
                s.setSignal( den )
                s.setConsSignal( consSignals )
            else:
                s.setCtrlSignal( den )
                s.setConsCtrlSignal( consSignals )
    #calculated = {}
    #for i in range( count ):
    #    print i
    #    chrom, stitchedChrom = outQ.get()
    #    calculated[chrom] = stitchedChrom
    sam.close()


def writeStitched( outfilename, stitched, rankbyName, ctrlName ="NONE" ):
    '''
    Write the data for ROSE_callSuper.R
    REGION_ID    CHROM    START    STOP    NUM_LOCI    CONSTITUENT_SIZE    SNU16_H3K27AC_merged.bam    SNU16_Total_Input_ATCACG_lane8_read1
    '''
    header = ["REGION_ID","CHROM","START","STOP","NUM_LOCI","CONSTITUENT_SIZE", rankbyName, ]
    if ctrlName != "NONE":
        header.append( ctrlName )
    header.append("TOTAL_CONS_SIGNAL")
    if ctrlName != "NONE":
        header.append("TOTAL_CONS_CTRL")
    out = open(outfilename, 'w')
    out.write('\t'.join(header))
    out.write('\n')
    for chrom in stitched:
        for s in stitched[ chrom ]:
            line = [ s.getName(), chrom, s.getStart(), s.getEnd(), s.getNumLoci(), s.getTotalConsSize(), s.getSignal()]
            if ctrlName != "NONE":
                line.append( s.getCtrlSignal() )
            line.append( s.getTotalConsSignal() )
            if ctrlName != "NONE":
                line.append( s.getTotalConsCtrlSignal() )
            out.write( "\t".join(map(str, line) ) )
            out.write( '\n' )
    out.close()

def writeCons( outfilename, cons ):
    '''
    Write the peaks to a file.
    '''
    out = open( outfilename, 'w' )
    for chrom in cons:
        for c in cons[chrom]:
            line = [ chrom, c.getStart(), c.getEnd(), c.getName(), "0", c.getStrand() ]
            out.write('\t'.join(map(str,line)))
            out.write('\n')
    out.close()


def main():
    args = parseArgs()
    '''

    parser.add_argument("i", "input", dest="input",
                        help = "The input file in BED format.")
    parser.add_argument("r", "ref", dest="ref",
                        help = "The reference build. Can be [hg19, hg18, mm9, mm8].")
    parser.add_argument("b", "bam", dest="bam",
                        help = "The bam file to calculate the signal.")
    parser.add_argument("-c", "--control", dest="control",
                        help = "The control bam file to calculate background signal.")
    parser.add_argument("-o", "--output", dest="output", default="New",
                        help = "The output prefix, default is 'New'.")
    '''

    loci = loadBed( args.input )
    if args.exclude > 0:
        tss2000 = loadTSS( os.path.join( os.path.dirname(os.path.realpath(__file__)), "annotation/%s_refseq.ucsc" % args.ref), header=True, extension=args.exclude )
    else:
        tss2000 = None
    tss50 = loadTSS( os.path.join( os.path.dirname(os.path.realpath(__file__)), "annotation/%s_refseq.ucsc" % args.ref), header=True, extension=50 )


    def processPart( elements, window, tss=None, outputStr = "NEW", getCons = False ):
        e_stitched = stitchLoci( elements, stitchWindow = window, tss = tss )
        calculateSignal( e_stitched, args.bam, 200, isCtrl = False)
        ctrlName = "NONE"
        if args.control:
            print "has control"
            calculateSignal( e_stitched, args.control, 200, isCtrl = True)
            ctrlName = os.path.basename( args.control )
        writeCons( outputStr + "_cons.bed", elements )
        writeStitched( outputStr + ".txt", e_stitched, os.path.basename(args.bam), ctrlName = ctrlName )

        outFolder = "./"
        if  "/" in outputStr:
            outFolder = os.path.dirname( outputStr )
            if outFolder[-1] != '/':
                outFolder += "/"
        # outputStr = os.path.basename( outputStr )
        # cmd = 'R --no-save %s %s %s %s < %s/ROSE_callSuper.R' % (outFolder, outputStr+".txt", outputStr, ctrlName, os.path.dirname(os.path.realpath(__file__)))
        cmd = 'R --no-save %s %s %s %s < %s/ROSE_callSuper.R' % (outFolder, outputStr+".txt", os.path.basename( outputStr ), ctrlName, os.path.dirname(os.path.realpath(__file__)))
        os.system(cmd)
        if getCons:
            cmd = 'intersectBed -a %s -b %s%s_Gateway_SuperEnhancers.bed -u > %s_Super_cons.bed' % ( outputStr + "_cons.bed", outFolder, outputStr, outFolder + outputStr )
            os.system(cmd)
            cmd = 'intersectBed -a %s -b %s%s_Gateway_SuperEnhancers.bed -v > %s_Typical_cons.bed' % ( outputStr + "_cons.bed", outFolder, outputStr, outFolder + outputStr )
            os.system(cmd)
        return outFolder, outputStr
    #nonPromoter, promoter = removeLociOverlapRef( loci, tss2000, fraction = args.fraction )
    #processPart( nonPromoter, 12500, tss50, args.output + "_Enhancers", getCons=True )
    #processPart( promoter, 4000, outputStr = args.output + "_Promoters", getCons=True )
    def intersectCons(cons_file, refFile, outFile):
        cmd = 'intersectBed -a %s -b %s -u > %s' % (cons_file, refFile, outFile)
        os.system( cmd )

    outFolder, outputStr = processPart( loci, args.window, tss=tss2000, outputStr = args.output )
    separatePromoter = args.separate
    if separatePromoter:
        thisRefName = args.output + "_hg19_tss.bed"
        writeCons( thisRefName, tss50 )
        cmd = 'intersectBed -a %s%s_Gateway_SuperEnhancers.bed -b %s -u  > %s_SP.bed' % ( outFolder, outputStr, thisRefName, args.output )
        os.system( cmd )
        cmd = 'intersectBed -a %s%s_Gateway_SuperEnhancers.bed -b %s -v  > %s_SE.bed' % ( outFolder, outputStr, thisRefName, args.output )
        os.system( cmd )
        cmd = 'intersectBed -a %s%s_Gateway_Enhancers.bed -b %s -u | intersectBed -a stdin -b %s_SP.bed -v > %s_TP.bed' % ( outFolder, outputStr, thisRefName, args.output, args.output )
        os.system( cmd )
        cmd = 'intersectBed -a %s%s_Gateway_Enhancers.bed -b %s -v | intersectBed -a stdin -b %s_SE.bed -v > %s_TE.bed' % ( outFolder, outputStr, thisRefName, args.output, args.output )
        os.system( cmd )
        cons_file = args.output + "_cons.bed"
        refFiles = ["%s_%s.bed" % (args.output, i) for i in ["SP", "TP", "SE", "TE"]]
        outFiles = ["%s_%s_cons.bed" % (args.output, i) for i in ["SP", "TP", "SE", "TE"]]
        for refFile, outFile in zip(refFiles, outFiles):
            intersectCons(cons_file, refFile, outFile)
    #stitched = stitchLoci( nonPromoter, stitchWindow = 12500, tss = tss50 )
    #calculateSignal( stitched, args.bam, 200, isCtrl = False)
    #ctrlName = "NONE"
    #if args.control:
    #    print "has control"
    #    calculateSignal( stitched, args.control, 200, isCtrl = True)
    #    ctrlName = os.path.basename( args.control )
    #
    #writeStitched( args.output, stitched, os.path.basename(args.bam), ctrlName = ctrlName )
    #
    #promoter_stitched = stitchLoci( promoter, stitchWindow=4000)
    #calculateSignal( promoter_stitched, args.bam, 200, isCtrl = False )
    #ctrlName = "NONE"
    #if args.control:
    #    print "has control"
    #    calculateSignal( stitched, args.control, 200, isCtrl = True)
    #    ctrlName = os.path.basename( args.control )

if __name__ == '__main__':
    main()
