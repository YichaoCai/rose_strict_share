## Description

This is a script that mimic the process of calling super-enhancers from ChIP-seq data in ROSE (Whyte, Warren A., et al. "Master transcription factors and mediator establish super-enhancers at key cell identity genes." Cell 153.2 (2013): 307-319.). It has also been used in (Cao, Fan, et al. "Super-enhancers and broad H3K4me3 domains form complex gene regulatory circuits involving chromatin interactions." Scientific reports 7.1 (2017): 2186.).


## System Requirements

- Python 2.7
- numpy 1.8.0
- scipy 0.13.3
- pysam 0.9.1.4
- R 3.4.4
- bedtools 2.17.0


## Installation guide

This is a standalong script. Please see `python my_rose.py -h` for full usage.


## Usage

The bam files needs to be sorted and indexed in order to be used in this script.

```
usage: Stitch peaks and call super-enhancer. [-h] [-c CONTROL] [-o OUTPUT]
                                             [-f FRACTION] [-w WINDOW]
                                             [-e EXCLUDE]
                                             [--separate SEPARATE]
                                             input ref bam

positional arguments:
  input                 The input file in BED format.
  ref                   The reference build. Can be [hg19, hg18, mm9, mm8].
  bam                   The bam file to calculate the signal.

optional arguments:
  -h, --help            show this help message and exit
  -c CONTROL, --control CONTROL
                        The control bam file to calculate background signal.
  -o OUTPUT, --output OUTPUT
                        The output prefix, default is 'New'.
  -f FRACTION, --fraction FRACTION
                        The overlapping fraction (only those with > fraction
                        will be retained.
  -w WINDOW, --window WINDOW
                        The window for stitching peaks. Default: 12500
  -e EXCLUDE, --exclude EXCLUDE
                        The size of exclusion zones (on each side) around the
                        TSS. Default: 2000
  --separate SEPARATE   Whether to separate Super-enhancers and typical-
                        enhancers based on their proximity to TSSes. Default:
                        False:
```

## Example usage:

### Example: Calling super peaks from K562 ChIP-seq data using a stitching window of 4kb.

`python my_rose.py -c ./data/controlK562.sorted.subSample.bam -o ./results/NEW -w 4000 ./data/wgEncodeBroadHistoneK562H3k27me3StdPk.sorted.broadPeak hg19 ./data/H3K27me3K562_rep1N2.sorted.subSample.bam`

Source of demo data: K562 ChIP-seq peaks from ENCODE UCSC (wgEncodeEH000044); ChIP-seq bam files from ENCODE (ENCSR000AKQ and ENCSR000AKY) bam files are merged, sorted and indexed. Subsample files are generated using `samtools view -s 0.1` to sample 10% of reads in the BAM files for test run.


### Output:

Outputs in `./results/`:

- "NEW_AllEnhancer.table.txt": Peaks after stitching with classification of super or typical peaks.
- "New_cons.bed": All the constituent peaks (of the super peaks).
- "New_Enhancers_withSuper.bed": All super peaks and typical peaks in BED format.
- "New_Gateway_Enhancer.bed": All super peaks in BED format.
- "New_Gateway_Enhancer.bed": All typical peaks in BED format.
- "New_SuperEnhancers.table.txt": All the super peaks.
- "New_Plot_points.png": Signal curve of stitched peaks in the calling process.
- "New.txt": Text file of "NEW_AllEnhancer.table.txt" contains information of all peaks without headers. 



